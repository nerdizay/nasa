const buttonLeft = document.querySelector('.arrow_left')
const buttonRight = document.querySelector('.arrow_right')
const previewContainer = document.querySelector('.preview_container')

const mainImages = document.querySelectorAll('.main_image')

let currentVisibleMainImage = mainImages[0]
currentVisibleMainImage.style.display = 'block'

buttonRight.addEventListener('click', () => previewContainer.scrollBy((165+30)*5, 0))
buttonLeft.addEventListener('click', () => previewContainer.scrollBy((-165-30)*5, 0))

previewContainer.addEventListener('click', (e) => {
    if (e.target.classList.contains('preview_image')) {
        currentVisibleMainImage.style.display = 'none'
        currentVisibleMainImage = document.querySelector(`#main-${e.target.id.split('-')[1]}`)
        currentVisibleMainImage.style.display = 'block'
    }
})

const modalWindow = document.querySelector('.modal_window')
const closeModalWindowBtn = document.querySelector('.close_modal_window_btn')

modalWindow.style.display = 'none'
closeModalWindowBtn.addEventListener('click', () => modalWindow.style.display = 'none')
document.addEventListener('click', (e) => {
    if (e.target.classList.contains('main_image')) {
        modalWindow.style.display = 'block'
        modalWindow.scrollIntoView(true)
    }
    
})


