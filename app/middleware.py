from filer.models import File
from icecream import ic
from django.core.handlers.wsgi import WSGIRequest
from django.http.response import JsonResponse
from .models import Slider


class SaveImageMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: WSGIRequest):
        
        if not (request.method == 'POST' and
            '/admin/filer/clipboard/operations/upload' in request.path
        ):
            response = self.get_response(request)
            return response
        
        count_files_before = File.objects.count()
        response: JsonResponse = self.get_response(request)
        
        if response.status_code !=200:
            return response
        
        files: list[File] = list(File.objects.all()[count_files_before:])
        sliders = [Slider(file=file) for file in files]
        Slider.objects.bulk_create(sliders)
        
        return response

            
        