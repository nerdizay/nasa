from django.shortcuts import render
from .models import Slider

def index(request):
    slider = Slider.objects.all()
    return render(request, 'index.html', {"images": slider})
