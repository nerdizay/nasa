from django.db import models
from filer.models import File


def get_number_of_last_image():
    """для модели Slider"""
    sliders = Slider.objects.order_by('-custom_order')
    if sliders.exists():
        return sliders.first().custom_order + 1
    else:
        return 0


class Slider(models.Model):
    custom_order = models.PositiveIntegerField(
        default=get_number_of_last_image,
        blank=False,
        null=False,
        db_index=True,
        verbose_name="порядок"
    )
    file = models.OneToOneField(File, to_field='id', on_delete=models.CASCADE, null=True)
    class Meta:
        db_table = "slider"
        verbose_name = "Картинка"
        verbose_name_plural = "Слайдер"
        ordering = ['custom_order']

    def __str__(self):
        return "Слайдер"