from django.contrib import admin
from adminsortable2.admin import SortableAdminMixin
from django.utils.html import format_html
from .models import Slider


@admin.register(Slider)
class SliderAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('custom_order', 'preview', 'filename')
    fields = ("filename", "image")
    readonly_fields = ("filename", "image")
    
    def preview(self, obj):
        image = obj.file.thumbnails['admin_directory_listing_icon']
        return format_html(f'<img src="{image}">')
    
    def image(self, obj):
        image = obj.file.thumbnails['admin_sidebar_preview']
        return format_html(f'<img src="{image}">')
    
    def filename(self, obj):
        return obj.file.original_filename
    
    def has_delete_permission(self, request, obj=None):
        return False

    preview.short_description = 'Картинка'
    image.short_description = 'Картинка'
    filename.short_description = 'Название'


admin.site.site_header = "Админ панель"
